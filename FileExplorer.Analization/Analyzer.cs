﻿using System;
using System.Linq;
using FileExplorer.Core.Attributes;
using FileExplorer.Core.FileSystemInfoProvider;

namespace FileExplorer.Analization
{
    public class Analyzer
    {
        public string GetFileSystemName(IFileSystemInfoProvider provider)
        {
            Type type = provider.GetType();
            object[] attr = type.GetCustomAttributes(typeof (FileSystemNameAttribute), false);
            FileSystemNameAttribute fileSystemNameAttribute = attr.Cast<FileSystemNameAttribute>()
                .FirstOrDefault();
            if (fileSystemNameAttribute != null)
            {
                return fileSystemNameAttribute.Name;
            }
            return String.Empty;
        }
    }
}