﻿using System;
using FileExplorer.Core.FileSystemItems;

namespace FileExplorer.InfinityFileSystem
{
    public class VirtualDirectoryItem : IDirectoryItem
    {
        public VirtualDirectoryItem(FileSystemItemInfo fileSystemItemInfo)
        {
            if (fileSystemItemInfo != null)
            {
                this.SetItemInfo(fileSystemItemInfo);
            }
        }

        public IFileSystemItem Parent { get; private set; }

        public string Label { get; private set; }

        public string Name { get; set; }

        public string Description { get; private set; }

        public string FullPath { get; private set; }

        public long Size { get; set; }

        public DateTime CreationTime { get; private set; }

        public DateTime UpdationTime { get; private set; }

        public bool IsReady()
        {
            return true;
        }

        private void SetItemInfo(FileSystemItemInfo info)
        {
            this.Parent = info.Parent;
            this.Label = info.Label;
            this.Name = info.Name;
            this.Description = info.Description;
            this.FullPath = info.FullPath;
            this.CreationTime = info.CreationTime;
            this.UpdationTime = info.LastUpdateTime;
            this.Size = info.Size;
        }
    }
}