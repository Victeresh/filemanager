﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Threading;
using FileExplorer.Core.Attributes;
using FileExplorer.Core.FileSystemInfoProvider;
using FileExplorer.Core.FileSystemItems;

namespace FileExplorer.InfinityFileSystem
{
    /// <summary>
    ///     Cheap lazy solution, just for demonstration.
    ///     You should be surprised when tree view will give you just 5 directories for parent directory, but file explorer
    ///     control gives you 50 000.
    /// </summary>
    [FileSystemName(Name = "Virtual File System")]
    [Export(typeof (IFileSystemInfoProvider))]
    public class InfiniteFileSystemProvider : IFileSystemInfoProvider
    {
        private readonly object lockObject = new object();

        [ImportingConstructor]
        public InfiniteFileSystemProvider()
        {
        }

        public IEnumerable<IFileSystemItem> GetChildsForItem(IFileSystemItem parent)
        {
            return this.GetChildsForItem(parent, 0, 20);
        }

        public bool IsThereAChild(IFileSystemItem parent)
        {
            return true;
        }

        /// <summary>
        ///     There is no filter using.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public IEnumerable<IFileSystemItem> GetChildsForItem(IFileSystemItem parent, Func<IFileSystemItem, bool> filter)
        {
            string parentPath = parent == null ? String.Empty : parent.FullPath;
            return GetDirectories(parent, 0, 10, parentPath);
        }

        public IEnumerable<IFileSystemItem> GetChildsForItem(IFileSystemItem parent, int startIndex, int pageSize)
        {
            Thread.Sleep(3000);
            var data = new List<IFileSystemItem>();
            string fullPath = parent == null ? String.Empty : parent.FullPath;
            data.AddRange(GetDirectories(parent, startIndex, pageSize, fullPath));
            for (int i = (pageSize/2); i < pageSize; i++)
            {
                string name = "File" + startIndex;
                string path = fullPath + "." + name;
                FileSystemItemInfo fileSystemItemInfo = FileSystemItemInfo(parent, path, name);
                var file =
                    new FileItem(fileSystemItemInfo);
                data.Add(file);
                startIndex++;
            }
            return data;
        }

        private static IEnumerable<IFileSystemItem> GetDirectories(IFileSystemItem parent, int startIndex, int pageSize,
            string fullPath)
        {
            var data = new List<IFileSystemItem>();
            for (int i = 0; i < pageSize/2; i++)
            {
                string name = "Directory" + startIndex;
                string path = fullPath + "." + name;
                var file =
                    new VirtualDirectoryItem(FileSystemItemInfo(parent, path, name));
                data.Add(file);
                startIndex++;
            }
            return data;
        }

        private static FileSystemItemInfo FileSystemItemInfo(IFileSystemItem parent, string path, string name)
        {
            return new FileSystemItemInfo(DateTime.Now, DateTime.Now, path, "some virtual file/directory", name,
                name, parent, 1024);
        }
    }
}