﻿using System;
using System.Collections.Generic;
using FileExplorer.Core.FileSystemItems;

namespace FileExplorer.Core.FileSystemInfoProvider
{
    public interface IFileSystemInfoProvider
    {
        /// <summary>
        ///     Return a child elements for specified parent item, for a null item it will get the root of a file system,
        /// </summary>
        /// <param name="parent">parent item</param>
        /// <returns></returns>
        IEnumerable<IFileSystemItem> GetChildsForItem(IFileSystemItem parent);

        //we dont want to load all the childs for each node but we need to provide to a treeView that we have some childs to display, 
        //otherwise it will not give us expanding ability.
        bool IsThereAChild(IFileSystemItem parent);

        IEnumerable<IFileSystemItem> GetChildsForItem(IFileSystemItem parent, Func<IFileSystemItem, bool> filter);

        /// <summary>
        ///     There will be some UI hanging when we provide a lot of items (like 100 000) to some ItemsControl (items anyway will
        ///     be loaded async. way) ,
        ///     to prevent it we should use some paging ability with data virtualization, it's partly implemented here, but we
        ///     don't use it on UI side.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="startIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        IEnumerable<IFileSystemItem> GetChildsForItem(IFileSystemItem parent, int startIndex, int pageSize);
    }
}