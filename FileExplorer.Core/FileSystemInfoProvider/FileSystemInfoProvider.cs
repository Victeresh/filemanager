﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using FileExplorer.Core.Attributes;
using FileExplorer.Core.FileSystemItemFactory;
using FileExplorer.Core.FileSystemItems;

namespace FileExplorer.Core.FileSystemInfoProvider
{
    [FileSystemName(Name = "Local File System")]
    [Export(typeof (LocalFileSystemInfoProvider))]
    [Export(typeof (IFileSystemInfoProvider))]
    public class LocalFileSystemInfoProvider : IFileSystemInfoProvider
    {
        private readonly IFileSystemItemFactory factory;

        /// <summary>
        ///     less memory, more speed.
        /// </summary>
        private readonly Dictionary<string, List<IFileSystemItem>> hashedFileSystemItems =
            new Dictionary<string, List<IFileSystemItem>>();

        private readonly object lockObject = new object();

        [ImportingConstructor]
        public LocalFileSystemInfoProvider(IFileSystemItemFactory factory)
        {
            this.factory = factory;
        }

        public IEnumerable<IFileSystemItem> GetChildsForItem(IFileSystemItem parent)
        {
            string path = parent == null ? String.Empty : parent.FullPath;
            if (this.hashedFileSystemItems.ContainsKey(path))
            {
                return this.hashedFileSystemItems[path];
            }

            IEnumerable<IFileSystemItem> childs = this.factory.GetChildsForItem(parent);
            this.hashedFileSystemItems.Add(path, childs.ToList());

            return this.hashedFileSystemItems[path];
        }

        public bool IsThereAChild(IFileSystemItem parent)
        {
            //TODO
            return true;
        }

        public IEnumerable<IFileSystemItem> GetChildsForItem(IFileSystemItem parent, Func<IFileSystemItem, bool> filter)
        {
            return this.GetChildsForItem(parent)
                .Where(filter);
        }

        /// <summary>
        /// Thread safe, or should be.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="startIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public IEnumerable<IFileSystemItem> GetChildsForItem(IFileSystemItem parent, int startIndex, int pageSize)
        {
            string path = parent == null ? String.Empty : parent.FullPath;

            lock (this.lockObject)
            {
                if (this.hashedFileSystemItems.ContainsKey(path))
                    // critical moment, the same key already exist exception.
                {
                    return this.hashedFileSystemItems[path];
                }

                IEnumerable<IFileSystemItem> childs = this.factory.GetChildsForItem(parent);
                this.hashedFileSystemItems.Add(path, childs.ToList()); // critical moment
            }

            if (this.hashedFileSystemItems[path].Count - startIndex - pageSize < 0)
            {
                pageSize = this.hashedFileSystemItems[path].Count - startIndex;
            }
            return this.hashedFileSystemItems[path].GetRange(startIndex, pageSize);
        }
    }
}