﻿using System;

namespace FileExplorer.Core.Attributes
{
    public class FileSystemNameAttribute : Attribute
    {
        public string Name { get; set; }
    }
}