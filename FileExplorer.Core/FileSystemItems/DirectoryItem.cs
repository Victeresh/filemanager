﻿using System;
using System.IO;

namespace FileExplorer.Core.FileSystemItems
{
    public class DirectoryItem : IDirectoryItem
    {
        private readonly DirectoryInfo directoryInfo;

        public DirectoryItem(FileSystemItemInfo info, DirectoryInfo directoryInfo)
        {
            this.directoryInfo = directoryInfo;
            if (info != null)
            {
                this.SetItemInfo(info);
            }
        }

        public IFileSystemItem Parent { get; set; }

        public string Label { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string FullPath { get; set; }

        public long Size { get; set; }

        public DateTime CreationTime { get; set; }

        public DateTime UpdationTime { get; set; }

        public bool IsReady()
        {
            if ((this.directoryInfo.Attributes & FileAttributes.Hidden) == 0)
            {
                return true;
            }
            return false;
        }

        private void SetItemInfo(FileSystemItemInfo info)
        {
            this.Parent = info.Parent;
            this.Label = info.Label;
            this.Name = info.Name;
            this.Description = info.Description;
            this.FullPath = info.FullPath;
            this.CreationTime = info.CreationTime;
            this.UpdationTime = info.LastUpdateTime;
            this.Size = info.Size;
        }
    }

    public interface IDirectoryItem : IFileSystemItem
    {
    }
}