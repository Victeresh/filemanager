using System;

namespace FileExplorer.Core.FileSystemItems
{
    public class FileSystemItemInfo
    {
        public FileSystemItemInfo(DateTime lastUpdateTime, DateTime creationTime, string fullPath, string description,
            string name, string label, IFileSystemItem parent, long size)
        {
            this.LastUpdateTime = lastUpdateTime;
            this.CreationTime = creationTime;
            this.FullPath = fullPath;
            this.Description = description;
            this.Name = name;
            this.Label = label;
            this.Parent = parent;
            this.Size = size;
        }

        public IFileSystemItem Parent { get; private set; }

        public string Label { get; private set; }

        public string Name { get; set; }

        public string Description { get; private set; }

        public string FullPath { get; private set; }

        public DateTime CreationTime { get; private set; }

        public DateTime LastUpdateTime { get; private set; }

        public long Size { get; private set; }
    }
}