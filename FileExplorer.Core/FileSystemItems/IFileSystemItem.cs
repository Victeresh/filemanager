﻿using System;

namespace FileExplorer.Core.FileSystemItems
{
    public interface IFileSystemItem
    {
        IFileSystemItem Parent { get; }

        string Label { get; }

        string Name { get; set; }

        string Description { get; }

        string FullPath { get; }

        long Size { get; set; }

        DateTime CreationTime { get; }

        DateTime UpdationTime { get; }

        bool IsReady();
    }
}