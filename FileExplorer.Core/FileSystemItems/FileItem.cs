using System;

namespace FileExplorer.Core.FileSystemItems
{
    public class FileItem : IFileSystemItem
    {
        public FileItem(FileSystemItemInfo info)
        {
            if (info != null)
            {
                this.SetItemInfo(info);
            }
        }

        public IFileSystemItem Parent { get; set; }

        public string Label { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string FullPath { get; set; }

        public long Size { get; set; }

        public DateTime CreationTime { get; set; }

        public DateTime UpdationTime { get; set; }

        public bool IsReady()
        {
            return true;
        }

        private void SetItemInfo(FileSystemItemInfo info)
        {
            this.Parent = info.Parent;
            this.Label = info.Label;
            this.Name = info.Name;
            this.Description = info.Description;
            this.FullPath = info.FullPath;
            this.CreationTime = info.LastUpdateTime;
            this.Size = info.Size;
        }
    }
}