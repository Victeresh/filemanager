using System;
using System.IO;

namespace FileExplorer.Core.FileSystemItems
{
    public class VolumeItem : IFileSystemItem
    {
        private readonly DriveInfo drive;

        public VolumeItem(FileSystemItemInfo info, DriveInfo drive)
        {
            this.drive = drive;
            if (info != null)
            {
                this.SetItemInfo(info);
            }
        }

        public IFileSystemItem Parent { get; private set; }

        public string Label { get; private set; }

        public string Name { get; set; }

        public string Description { get; private set; }

        public string FullPath { get; private set; }

        public long Size { get; set; }

        public DateTime CreationTime { get; private set; }

        public DateTime UpdationTime { get; private set; }

        public bool IsReady()
        {
            return this.drive.IsReady;
        }

        private void SetItemInfo(FileSystemItemInfo info)
        {
            this.Parent = info.Parent;
            this.Label = info.Label;
            this.Name = info.Name;
            this.Description = info.Description;
            this.FullPath = info.FullPath;
            this.CreationTime = info.CreationTime;
            this.Size = info.Size;
            this.UpdationTime = info.LastUpdateTime;
        }
    }
}