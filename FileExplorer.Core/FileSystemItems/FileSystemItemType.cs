﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeeamFileExplorer.Core.FileSystemItems
{
    enum FileSystemItemType
    {
        File,
        Directory,
        Volume
    }
}
