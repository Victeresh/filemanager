using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using FileExplorer.Core.FileSystemItems;

namespace FileExplorer.Core.FileSystemItemFactory
{
    [Export(typeof (IFileSystemItemFactory))]
    internal class FileSystemItemFactory : IFileSystemItemFactory
    {
        private readonly Dictionary<FileSystemItemType, Func<string, IFileSystemItem, IFileSystemItem>>
            factoryDictionary = new Dictionary<FileSystemItemType, Func<string, IFileSystemItem, IFileSystemItem>>();

        public FileSystemItemFactory()
        {
            this.factoryDictionary.Add(FileSystemItemType.Directory, this.CreateDirectoryItem);
            this.factoryDictionary.Add(FileSystemItemType.File, this.CreateFileItem);
            this.factoryDictionary.Add(FileSystemItemType.Volume, this.CreateVolumeItem);
        }

        public IFileSystemItem GetFileSystemItem(string path, IFileSystemItem parent)
        {
            if (parent == null) // not so good with factoryDictionary actually.
            {
                return this.factoryDictionary[FileSystemItemType.Volume](path, parent);
            }

            FileAttributes pathData = File.GetAttributes(path);
            if ((pathData & FileAttributes.Directory) == FileAttributes.Directory)
            {
                return this.factoryDictionary[FileSystemItemType.Directory](path, parent);
            }

            return this.factoryDictionary[FileSystemItemType.File](path, parent);
        }

        public IEnumerable<IFileSystemItem> GetChildsForItem(IFileSystemItem parent)
        {
            var items = new List<IFileSystemItem>();
            bool canBeOpened = ((parent != null) && !parent.IsReady());
            if ((parent is FileItem) || canBeOpened)
            {
                return items;
            }

            IEnumerable<string> directoryPaths = parent == null
                ? DriveInfo.GetDrives()
                    .Select(i => i.Name)
                : Directory.EnumerateDirectories(parent.FullPath);

            IEnumerable<string> filePaths = parent == null ? new string[] {} : Directory.EnumerateFiles(parent.FullPath);
            items = directoryPaths.Select(path => this.GetFileSystemItem(path, parent))
                .ToList();

            IEnumerable<IFileSystemItem> files = filePaths.Select(path => this.GetFileSystemItem(path, parent));
            items.AddRange(files);

            return items;
        }

        private IFileSystemItem CreateVolumeItem(string path, IFileSystemItem parent = null)
        {
            DriveInfo drive = DriveInfo.GetDrives()
                .FirstOrDefault(i => i.Name == path);

            var info = new FileSystemItemInfo(new DateTime(), new DateTime(), path, "It's a volume", drive.Name,
                drive.Name, parent, drive.IsReady ? drive.TotalSize : 0);
            return new VolumeItem(info, drive);
        }

        private IFileSystemItem CreateDirectoryItem(string path, IFileSystemItem parent)
        {
            DateTime creationTime = Directory.GetCreationTime(path);
            DateTime lastUpdTime = Directory.GetLastWriteTime(path);
            var directoryInfo = new DirectoryInfo(path);
            string name = directoryInfo.Name;
            var info = new FileSystemItemInfo(lastUpdTime, creationTime, path, "It's a directory", name, name, parent, 0);
            return new DirectoryItem(info, directoryInfo);
        }

        private IFileSystemItem CreateFileItem(string path, IFileSystemItem parent)
        {
            DateTime creationTime = Directory.GetCreationTime(path);
            DateTime lastUpdTime = Directory.GetLastWriteTime(path);
            string name = Path.GetFileNameWithoutExtension(path);
            long length = new FileInfo(path).Length;
            var info = new FileSystemItemInfo(lastUpdTime, creationTime, path, "It's a file", name, name, parent, length);
            return new FileItem(info);
        }
    }
}