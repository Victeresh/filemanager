namespace FileExplorer.Core.FileSystemItemFactory
{
    public enum FileSystemItemType
    {
        Volume,
        Directory,
        File
    }
}