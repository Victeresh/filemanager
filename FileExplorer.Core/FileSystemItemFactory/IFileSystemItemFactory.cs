﻿using System.Collections.Generic;
using FileExplorer.Core.FileSystemItems;

namespace FileExplorer.Core.FileSystemItemFactory
{
    public interface IFileSystemItemFactory
    {
        IFileSystemItem GetFileSystemItem(string path, IFileSystemItem parent);

        IEnumerable<IFileSystemItem> GetChildsForItem(IFileSystemItem parent);
    }
}