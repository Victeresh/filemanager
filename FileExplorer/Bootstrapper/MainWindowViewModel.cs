﻿using System.ComponentModel.Composition;
using Caliburn.Micro;
using FileExplorer.WpfApplication.ViewModels;

namespace FileExplorer.WpfApplication.Bootstrapper
{
    [Export(typeof (IShell))]
    public class MainWindowViewModel : PropertyChangedBase, IShell
    {
        private FileSystemTreeViewModel fileSystemTree;
        private ItemsExplorerViewModel itemsExplorer;
        private NavigationViewModel navigation;
        private StatusBarViewModel status;

        [ImportingConstructor]
        public MainWindowViewModel(IEventAggregator aggregator)
        {

        }

        [Import]
        public ItemsExplorerViewModel ItemsExplorer
        {
            get { return this.itemsExplorer; }
            set
            {
                if (Equals(value, this.itemsExplorer))
                {
                    return;
                }
                this.itemsExplorer = value;
                NotifyOfPropertyChange(() => this.ItemsExplorer);
            }
        }

        [Import]
        public FileSystemTreeViewModel FileSystemTree
        {
            get { return this.fileSystemTree; }
            set
            {
                if (Equals(value, this.fileSystemTree))
                {
                    return;
                }
                this.fileSystemTree = value;
                NotifyOfPropertyChange(() => this.FileSystemTree);
            }
        }

        [Import]
        public NavigationViewModel Navigation
        {
            get { return this.navigation; }
            set
            {
                if (Equals(value, this.navigation))
                {
                    return;
                }
                this.navigation = value;
                NotifyOfPropertyChange(() => this.Navigation);
            }
        }

        [Import]
        public StatusBarViewModel Status
        {
            get { return this.status; }
            set
            {
                if (Equals(value, this.status))
                {
                    return;
                }
                this.status = value;
                NotifyOfPropertyChange(() => this.Status);
            }
        }
    }
}