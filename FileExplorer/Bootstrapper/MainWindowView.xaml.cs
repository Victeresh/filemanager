﻿using System.Windows;

namespace FileExplorer.WpfApplication.Bootstrapper
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindowView : Window
    {
        public MainWindowView()
        {
            this.InitializeComponent();
        }
    }
}