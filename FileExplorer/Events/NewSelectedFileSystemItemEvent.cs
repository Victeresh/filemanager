﻿using FileExplorer.Core.FileSystemItems;

namespace FileExplorer.WpfApplication.Events
{
    public class NewSelectedFileSystemItemEvent
    {
        private readonly IFileSystemItem item;

        public NewSelectedFileSystemItemEvent(IFileSystemItem item)
        {
            this.item = item;
        }

        public IFileSystemItem Item
        {
            get { return this.item; }
        }
    }
}