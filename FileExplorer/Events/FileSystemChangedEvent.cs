﻿using FileExplorer.Core.FileSystemInfoProvider;

namespace FileExplorer.WpfApplication.Events
{
    public class FileSystemChangedEvent
    {
        public IFileSystemInfoProvider Provider { get; set; }
    }
}