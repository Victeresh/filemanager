﻿namespace FileExplorer.WpfApplication.Events
{
    public class StatusMessageEvent
    {
        public string Message { get; set; }
    }
}