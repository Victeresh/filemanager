using System;
using System.Windows;
using System.Windows.Controls;
using FileExplorer.Core.FileSystemItemFactory;
using FileExplorer.WpfApplication.ViewModels;

namespace FileExplorer.WpfApplication.UICommon
{
    public class FileItemTemplateSelector : DataTemplateSelector
    {
        public DataTemplate VolumeDataTemplate { get; set; }

        public DataTemplate FileDataTemple { get; set; }

        public DataTemplate DirectoryDataTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item == null)
            {
                return base.SelectTemplate(item, container);
            }

            var itemViewModel = item as FileSystemItemViewModel;
            if (itemViewModel == null)
            {
                throw new ArgumentNullException("treeItemVm" +
                                                " in FileItemTemplateSelector");
            }
            switch (itemViewModel.Type)
            {
                case FileSystemItemType.Directory:
                    return this.DirectoryDataTemplate;
                case FileSystemItemType.File:
                    return this.FileDataTemple;
                case FileSystemItemType.Volume:
                    return this.VolumeDataTemplate;
            }
            return base.SelectTemplate(item, container);
        }
    }
}