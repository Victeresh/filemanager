﻿using System.Windows.Controls;

namespace FileExplorer.WpfApplication.Views
{
    /// <summary>
    ///     Interaction logic for FileSystemTreeView.xaml
    /// </summary>
    public partial class FileSystemTreeView : UserControl
    {
        public FileSystemTreeView()
        {
            this.InitializeComponent();
        }
    }
}