﻿using System.Windows.Controls;

namespace FileExplorer.WpfApplication.Views
{
    /// <summary>
    ///     Interaction logic for ItemsExplorerViewModel.xaml
    /// </summary>
    public partial class ItemsExplorerView : UserControl
    {
        public ItemsExplorerView()
        {
            this.InitializeComponent();
        }
    }
}