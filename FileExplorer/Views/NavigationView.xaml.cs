﻿using System.Windows.Controls;

namespace FileExplorer.WpfApplication.Views
{
    /// <summary>
    ///     Interaction logic for NavigationView.xaml
    /// </summary>
    public partial class NavigationView : UserControl
    {
        public NavigationView()
        {
            this.InitializeComponent();
        }
    }
}