﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Windows;
using System.Windows.Media.Imaging;
using Caliburn.Micro;
using FileExplorer.Core.FileSystemInfoProvider;
using FileExplorer.Core.FileSystemItemFactory;
using FileExplorer.Core.FileSystemItems;
using Action = System.Action;

namespace FileExplorer.WpfApplication.ViewModels
{
    public delegate IEnumerable<FileSystemItemViewModel> LoadChilds(int index);

    public class FileSystemItemViewModel : PropertyChangedBase
    {
        private const int PageSize = 100000;
        private readonly IFileSystemItem item;
        private readonly IFileSystemInfoProvider provider;
        private readonly FileSystemItemType type;
        private ObservableCollection<FileSystemItemViewModel> children;
        private bool isExpanded;

        public FileSystemItemViewModel(IFileSystemItem item, IFileSystemInfoProvider provider)
        {
            this.item = item;
            this.provider = provider;
            this.type = item is VolumeItem
                ? FileSystemItemType.Volume
                : item is IDirectoryItem ? FileSystemItemType.Directory : FileSystemItemType.File;
        }

        public ObservableCollection<FileSystemItemViewModel> Children
        {
            get
            {
                if (this.children == null)
                {
                    this.children = new ObservableCollection<FileSystemItemViewModel>();
                    this.LoadChildsAsynDelegate(); //  this.LoadChildsForFileSystemItem();
                }
                return this.children;
            }
            set
            {
                if (Equals(value, this.children))
                {
                    return;
                }
                this.children = value;
                NotifyOfPropertyChange(() => this.Children);
            }
        }

        public IEnumerable<FileSystemItemViewModel> Directories
        {
            get { return this.GetDirectories(); }
        }

        public bool IsExpanded
        {
            get { return this.isExpanded; }
            set
            {
                if (value.Equals(this.isExpanded))
                {
                    return;
                }
                this.isExpanded = value;
                NotifyOfPropertyChange(() => this.Directories);
                NotifyOfPropertyChange(() => this.IsExpanded);
            }
        }

        public bool IsEnabled
        {
            get { return this.item.IsReady(); }
        }

        public string ItemName
        {
            get { return this.item.Name; }
            set
            {
                this.item.Name = value;
                NotifyOfPropertyChange(() => this.ItemName);
            }
        }

        public DateTime CreateDateTime
        {
            get { return this.item.CreationTime; }
        }

        public DateTime UpdateDateTime
        {
            get { return this.item.UpdationTime; }
        }

        public string Description
        {
            get { return this.item.Description; }
        }

        public string Label
        {
            get { return this.item.Label; }
        }

        public long Size
        {
            get { return this.item.Size; }
        }

        public FileSystemItemType Type
        {
            get { return this.type; }
        }

        public IFileSystemItem Item
        {
            get { return this.item; }
        }

        public BitmapSource FileIcon { get; set; }


        /// <summary>
        ///     Used by treeView
        /// </summary>
        /// <returns></returns>
        private IEnumerable<FileSystemItemViewModel> GetDirectories()
        {
            if (this.IsExpanded == false && this.children == null)
            {
                if (this.provider.IsThereAChild(this.item))
                    //we dont want to load all the childs for each node but we need to provide to a treeView that we have some childs to display, 
                    //otherwise it will not give us expanding ability.
                {
                    var dummyChild = new FileSystemItemViewModel(new FileItem(null), this.provider);
                    return new[] {dummyChild};
                }
                return null;
            }

            return this.provider.GetChildsForItem(this.item, i => !(i is FileItem))
                .Select(i => new FileSystemItemViewModel(i, this.provider));
        }

        #region Async Background loading

        private void AddToChildren(IEnumerable<FileSystemItemViewModel> childs)
        {
            childs.Apply(i => this.Children.Add(i));
        }

        //private async void LoadChildsForFileSystemItem()
        //{
        //    this.Children = new ObservableCollection<FileSystemItemViewModel>();
        //    var childs = await this.GetElems(0);
        //    this.AddToChildren(childs);
        //}

        /// <summary>
        ///     GetChildsForItem should be thread safe.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        //private async Task<IEnumerable<FileSystemItemViewModel>> GetElems(int index)
        //{
        //    return await Task.Run(() => this.provider.GetChildsForItem(this.Item, index, PageSize)
        //        .Select(i => new FileSystemItemViewModel(i, this.provider)));
        //}

        #region Async Delegates  background loading
        private void LoadChildsAsynDelegate()
        {
            LoadChilds asyncLoading = this.LoadItemsFromProvider;
            asyncLoading.BeginInvoke(0, this.ApplyToChildren, null);
        }

        private void ApplyToChildren(IAsyncResult ar)
        {
            var asyncResult = ar as AsyncResult;
            if (asyncResult != null)
            {
                var loadChilds = asyncResult.AsyncDelegate as LoadChilds;
                if (loadChilds == null)
                {
                    return;
                }
                IEnumerable<FileSystemItemViewModel> result = loadChilds.EndInvoke(ar);
                this.AddToChildrenFromAnotherThread(result);
            }
        }

        private IEnumerable<FileSystemItemViewModel> LoadItemsFromProvider(int index)
        {
            return this.provider.GetChildsForItem(this.Item, index, PageSize)
                .Select(i => new FileSystemItemViewModel(i, this.provider));
        }

        private void AddToChildrenFromAnotherThread(IEnumerable<FileSystemItemViewModel> childs)
        {
            Application.Current.Dispatcher.Invoke(new Action(() => childs.Apply(i => this.Children.Add(i))));
        }

        #endregion

        #endregion
    }
}