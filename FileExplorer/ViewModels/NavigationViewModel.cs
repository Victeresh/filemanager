﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using Caliburn.Micro;
using FileExplorer.Analization;
using FileExplorer.Core.FileSystemInfoProvider;
using FileExplorer.Core.FileSystemItems;
using FileExplorer.WpfApplication.Events;

namespace FileExplorer.WpfApplication.ViewModels
{
    [Export(typeof (NavigationViewModel))]
    public class NavigationViewModel : PropertyChangedBase, IHandle<NewSelectedFileSystemItemEvent>,
        IHandle<FileSystemChangedEvent>
    {
        private readonly IEventAggregator aggregator;

        #region FileSystems

        private readonly IEnumerable<IFileSystemInfoProvider> fileSystemProviders;

        private readonly Dictionary<string, IFileSystemInfoProvider> fileSystemsDictionary =
            new Dictionary<string, IFileSystemInfoProvider>();

        private string selectedFileSystem;

        public IEnumerable<string> FileSystems
        {
            get { return this.fileSystemsDictionary.Keys; }
        }

        public string SelectedFileSystem
        {
            get { return this.selectedFileSystem; }
            set
            {
                if (value == this.selectedFileSystem)
                {
                    return;
                }
                this.selectedFileSystem = value;
                this.aggregator.PublishOnUIThread(new FileSystemChangedEvent
                {
                    Provider = this.fileSystemsDictionary[value]
                });
                NotifyOfPropertyChange(() => this.SelectedFileSystem);
            }
        }

        private void AnalizeFileSystems()
        {
            var analizer = new Analyzer();
            foreach (IFileSystemInfoProvider provider in this.fileSystemProviders)
            {
                var fileSystemName = analizer.GetFileSystemName(provider);
                if (this.fileSystemsDictionary.ContainsKey(fileSystemName))
                {
                    throw new Exception("Dublicated file system name exception");
                }

                this.fileSystemsDictionary.Add(fileSystemName, provider);
            }
        }

        #endregion

        [ImportingConstructor]
        public NavigationViewModel(IEventAggregator aggregator,
            [ImportMany] IEnumerable<IFileSystemInfoProvider> fileSystemProviders)
        {
            this.aggregator = aggregator;
            this.fileSystemProviders = fileSystemProviders;
            this.AnalizeFileSystems();
            aggregator.Subscribe(this);
            this.SelectedFileSystem = this.fileSystemsDictionary.Keys.FirstOrDefault();
        }


        /// <summary>
        ///     Called by caliburn micro via name convention.
        /// </summary>
        public bool CanGoNext
        {
            get
            {
                if (this.nextStack.Count == 0)
                {
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        ///     Called by caliburn micro via name convention.
        /// </summary>
        public bool CanGoBack
        {
            get
            {
                if (this.backStack.Count == 0)
                {
                    return false;
                }
                return true;
            }
        }

        public void Handle(FileSystemChangedEvent message)
        {
            this.currentItem = null;
            this.backStack.Clear();
            this.nextStack.Clear();
            NotifyOfPropertyChange(() => this.CanGoBack);
            NotifyOfPropertyChange(() => this.CanGoNext);
        }

        public void Handle(NewSelectedFileSystemItemEvent message)
        {
            //To check in case we have called NewSelectedFileSystemItemEvent, may be we could use another message type instead of NewSelectedFileSystemItemEvent
            //so there would be no need to handle this situation..
            if (this.currentItem == message.Item)
            {
                return;
            }

            this.backStack.Push(this.currentItem);
            this.nextStack.Clear();
            this.currentItem = message.Item;
            NotifyOfPropertyChange(() => this.CanGoBack);
            NotifyOfPropertyChange(() => this.CanGoNext);
        }

        public void GoBack()
        {
            this.nextStack.Push(this.currentItem);
            this.currentItem = this.backStack.Pop();
            this.aggregator.PublishOnUIThread(new NewSelectedFileSystemItemEvent(this.currentItem));
            NotifyOfPropertyChange(() => this.CanGoBack);
            NotifyOfPropertyChange(() => this.CanGoNext);
        }

        public void GoNext()
        {
            this.backStack.Push(this.currentItem);
            this.currentItem = this.nextStack.Pop();
            this.aggregator.PublishOnUIThread(new NewSelectedFileSystemItemEvent(this.currentItem));
            NotifyOfPropertyChange(() => this.CanGoBack);
            NotifyOfPropertyChange(() => this.CanGoNext);
        }

        #region NavigationPart

        private readonly Stack<IFileSystemItem> backStack = new Stack<IFileSystemItem>();
        private readonly Stack<IFileSystemItem> nextStack = new Stack<IFileSystemItem>();
        private IFileSystemItem currentItem;

        #endregion
    }
}