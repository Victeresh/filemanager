﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using Caliburn.Micro;
using FileExplorer.Core.FileSystemInfoProvider;
using FileExplorer.Core.FileSystemItems;
using FileExplorer.WpfApplication.Events;

namespace FileExplorer.WpfApplication.ViewModels
{
    [Export(typeof (FileSystemTreeViewModel))]
    public class FileSystemTreeViewModel : PropertyChangedBase, IHandle<FileSystemChangedEvent>
    {
        private readonly IEventAggregator aggregator;
        private IFileSystemInfoProvider provider;
        private IEnumerable<FileSystemItemViewModel> rootItems;

        [ImportingConstructor]
        public FileSystemTreeViewModel(IEventAggregator aggregator)
        {
            this.aggregator = aggregator;
            aggregator.Subscribe(this);
        }

        public IEnumerable<FileSystemItemViewModel> RootItems
        {
            get { return this.rootItems; }
            set
            {
                if (Equals(value, this.rootItems))
                {
                    return;
                }
                this.rootItems = value;
                NotifyOfPropertyChange(() => this.RootItems);
            }
        }

        public void Handle(FileSystemChangedEvent message)
        {
            this.provider = message.Provider;

            this.RootItems = this.provider.GetChildsForItem(null, i => !(i is FileItem))
                .Select(i => new FileSystemItemViewModel(i, this.provider));
        }

        public void SelectedFSItemChanged(FileSystemItemViewModel viewModel)
        {
            if (viewModel != null)
            {
                this.aggregator.PublishOnUIThread(new NewSelectedFileSystemItemEvent(viewModel.Item));
            }
        }
    }
}