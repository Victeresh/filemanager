﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using Caliburn.Micro;
using FileExplorer.Core.FileSystemInfoProvider;
using FileExplorer.Core.FileSystemItemFactory;
using FileExplorer.WpfApplication.Events;
using FileExplorer.WpfApplication.Utils;

namespace FileExplorer.WpfApplication.ViewModels
{
    [Export(typeof (ItemsExplorerViewModel))]
    public class ItemsExplorerViewModel : PropertyChangedBase, IHandle<NewSelectedFileSystemItemEvent>,
        IHandle<FileSystemChangedEvent>
    {
        private readonly IEventAggregator aggregator;
        private readonly IFileIconProvider iconProvider;
        private IFileSystemInfoProvider fileSystemInfoProvider;
        private FileSystemItemViewModel parentItem;
        private FileSystemItemViewModel selectedItem;

        [ImportingConstructor]
        public ItemsExplorerViewModel(IEventAggregator aggregator,
            IFileIconProvider iconProvider)
        {
            this.aggregator = aggregator;
            this.iconProvider = iconProvider;
            this.aggregator.Subscribe(this);
        }

        public IEnumerable<FileSystemItemViewModel> FileSystemItems
        {
            get
            {
                if (this.parentItem == null)
                {
                    return null;
                }
                return this.parentItem.Children;
            }
        }

        public FileSystemItemViewModel SelectedItem
        {
            get { return this.selectedItem; }
            set
            {
                if (Equals(value, this.selectedItem))
                {
                    return;
                }
                this.selectedItem = value;
                NotifyOfPropertyChange(() => this.SelectedItem);
            }
        }

        public void Handle(FileSystemChangedEvent message)
        {
            this.fileSystemInfoProvider = message.Provider;
            this.parentItem = null;
            this.SelectedItem = null;
            NotifyOfPropertyChange(() => this.FileSystemItems);
        }

        public void Handle(NewSelectedFileSystemItemEvent message)
        {
            this.SetNewParentItem(new FileSystemItemViewModel(message.Item, this.fileSystemInfoProvider));
            foreach (FileSystemItemViewModel newItem in this.FileSystemItems)
                //TODO there is some bugwhen Icon is not initialized, 
            {
                newItem.FileIcon = this.iconProvider.GetIcon(newItem.Item.FullPath);
            }
        }

        public void ItemClickEvent(FileSystemItemViewModel clickedItem)
        {
            if (clickedItem.Type == FileSystemItemType.File)
            {
                Process.Start(clickedItem.Item.FullPath);
                return;
            }
            this.aggregator.PublishOnUIThread(new NewSelectedFileSystemItemEvent(clickedItem.Item));
            this.SetNewParentItem(clickedItem);
        }

        private void SetNewParentItem(FileSystemItemViewModel clickedItem)
        {
            this.parentItem = clickedItem;
            NotifyOfPropertyChange(() => this.FileSystemItems);
            if (this.parentItem.Item == null)
            {
                return;
            }
            this.aggregator.PublishOnUIThread(new StatusMessageEvent
            {
                Message = "Loading " + clickedItem.Item.FullPath
            });
            this.parentItem.Children.CollectionChanged += this.OnParentItemChildrenChanged;
        }

        private void OnParentItemChildrenChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            foreach (FileSystemItemViewModel newItem in e.NewItems.Cast<FileSystemItemViewModel>())
            {
                newItem.FileIcon = this.iconProvider.GetIcon(newItem.Item.FullPath);
            }
        }
    }
}