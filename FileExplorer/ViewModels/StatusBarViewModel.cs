﻿using System.ComponentModel.Composition;
using Caliburn.Micro;
using FileExplorer.WpfApplication.Events;

namespace FileExplorer.WpfApplication.ViewModels
{
    [Export(typeof (StatusBarViewModel))]
    public class StatusBarViewModel : PropertyChangedBase, IHandle<StatusMessageEvent>
    {
        private readonly IEventAggregator aggregator;
        private string status;

        [ImportingConstructor]
        public StatusBarViewModel(IEventAggregator aggregator)
        {
            this.aggregator = aggregator;
            this.aggregator.Subscribe(this);
        }

        public string Status
        {
            get { return this.status; }
            set
            {
                if (value == this.status)
                {
                    return;
                }
                this.status = value;
                NotifyOfPropertyChange(() => this.Status);
            }
        }

        public void Handle(StatusMessageEvent message)
        {
            this.Status = message.Message;
        }
    }
}