﻿using System;
using System.Windows;
using System.Windows.Threading;

namespace FileExplorer.WpfApplication
{
    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            Current.DispatcherUnhandledException += this.OnFailedTestTask;
        }

        private void OnFailedTestTask(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show("Sorry, i'm failed with exception handling =) " + Environment.NewLine + e.Exception);
        }
    }
}