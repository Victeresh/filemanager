﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Drawing;
using System.IO;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media.Imaging;

namespace FileExplorer.WpfApplication.Utils
{
    [Export(typeof (IFileIconProvider))]
    public class FileIconProvider : IFileIconProvider
    {
        /// <summary>
        ///     less memory, more speed.
        /// </summary>
        private readonly Dictionary<string, BitmapSource> iconFileDictionary = new Dictionary<string, BitmapSource>();

        public BitmapSource GetIcon(string filePath)
        {
            if (!File.Exists(filePath))
            {
                return null;
            }

            string fileExtension = Path.GetExtension(filePath);

            BitmapSource source;
            if (this.iconFileDictionary.TryGetValue(fileExtension, out source))
            {
                return source;
            }

            Icon icon = Icon.ExtractAssociatedIcon(filePath);
            if (icon != null)
            {
                source = Imaging.CreateBitmapSourceFromHIcon(icon.Handle,
                    Int32Rect.Empty,
                    BitmapSizeOptions.FromEmptyOptions());
                icon.Dispose();
                this.iconFileDictionary.Add(fileExtension, source);
                return source;
            }
            return null;
        }
    }
}