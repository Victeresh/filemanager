﻿using System.Windows.Media.Imaging;

namespace FileExplorer.WpfApplication.Utils
{
    public interface IFileIconProvider
    {
        BitmapSource GetIcon(string filePath);
    }
}